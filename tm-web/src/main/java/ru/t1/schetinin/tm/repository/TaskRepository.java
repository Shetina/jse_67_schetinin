package ru.t1.schetinin.tm.repository;

import org.jetbrains.annotations.NotNull;
import org.springframework.context.annotation.Scope;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;
import ru.t1.schetinin.tm.model.Project;
import ru.t1.schetinin.tm.model.Task;

import java.util.List;

@Repository
@Scope("prototype")
public interface TaskRepository extends JpaRepository<Task, String> {

    @NotNull
    List<Task> findByProject(@NotNull final Project project);

}