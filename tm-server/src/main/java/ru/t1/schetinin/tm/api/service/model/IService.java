package ru.t1.schetinin.tm.api.service.model;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.t1.schetinin.tm.enumerated.TMSort;
import ru.t1.schetinin.tm.model.AbstractModel;

import java.util.List;

public interface IService<M extends AbstractModel> {

    @Nullable
    List<M> findAll(@Nullable TMSort sort) throws Exception;

    void removeById(@Nullable String id) throws Exception;

    @NotNull
    M add(@NotNull M model) throws Exception;

    void clear() throws Exception;

    boolean existsById(@NotNull String id) throws Exception;

    @Nullable
    List<M> findAll() throws Exception;

    @Nullable
    M findOneById(@NotNull String id) throws Exception;

    int getSize() throws Exception;

    void remove(@NotNull M model) throws Exception;

    void update(@NotNull M model) throws Exception;

}