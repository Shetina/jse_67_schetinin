package ru.t1.schetinin.tm.log;

public enum OperationType {

    INSERT,
    UPDATE,
    DELETE;

}