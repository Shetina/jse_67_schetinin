package ru.t1.schetinin.tm.log;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@Getter
@Setter
@NoArgsConstructor
public final class OperationEvent {

    private OperationType type;

    private Object entity;

    private String table;

    private long timestamp = System.currentTimeMillis();

    public OperationEvent(final OperationType type, final Object entity) {
        this.type = type;
        this.entity = entity;
    }

}