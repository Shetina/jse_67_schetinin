package ru.t1.schetinin.tm.listener.project;

import org.jetbrains.annotations.Nullable;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import ru.t1.schetinin.tm.api.endpoint.IProjectEndpoint;
import ru.t1.schetinin.tm.listener.AbstractListener;
import ru.t1.schetinin.tm.enumerated.Status;
import ru.t1.schetinin.tm.dto.model.ProjectDTO;

@Component
public abstract class AbstractProjectListener extends AbstractListener {

    @Autowired
    protected IProjectEndpoint projectEndpoint;

    @Nullable
    @Override
    public String getArgument() {
        return null;
    }

    protected void showProject(@Nullable final ProjectDTO project) {
        if (project == null) return;
        System.out.println("ID: " + project.getId());
        System.out.println("NAME: " + project.getName());
        System.out.println("DESCRIPTION: " + project.getDescription());
        System.out.println("STATUS: " + Status.toName(project.getStatus()));
    }

}