package ru.t1.schetinin.tm.service;

import lombok.Getter;
import lombok.Setter;
import org.jetbrains.annotations.NotNull;
import org.springframework.stereotype.Service;
import ru.t1.schetinin.tm.api.service.ITokenService;

@Getter
@Setter
@Service
public class TokenService implements ITokenService {

    @NotNull
    private String token;

}