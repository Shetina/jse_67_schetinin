package ru.t1.schetinin.tm.repository.model;

import org.jetbrains.annotations.NotNull;
import org.springframework.stereotype.Repository;
import ru.t1.schetinin.tm.model.User;

@Repository
public interface IUserRepository extends IRepository<User> {

    @NotNull
    User findByLogin(@NotNull final String login);

    @NotNull
    User findByEmail(@NotNull final String email);

    boolean existsByLogin(@NotNull String login);

    boolean existsByEmail(@NotNull String email);

}