package ru.t1.schetinin.tm.service.dto;

import lombok.AllArgsConstructor;
import lombok.NoArgsConstructor;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import ru.t1.schetinin.tm.api.service.dto.ITaskDTOService;
import ru.t1.schetinin.tm.enumerated.Status;
import ru.t1.schetinin.tm.exception.entity.TaskNotFoundException;
import ru.t1.schetinin.tm.exception.field.DescriptionEmptyException;
import ru.t1.schetinin.tm.exception.field.IdEmptyException;
import ru.t1.schetinin.tm.exception.field.NameEmptyException;
import ru.t1.schetinin.tm.exception.field.StatusEmptyException;
import ru.t1.schetinin.tm.model.dto.TaskDTO;
import ru.t1.schetinin.tm.repository.TaskDTORepository;

import java.util.Collections;
import java.util.List;
import java.util.Optional;

@Service
@NoArgsConstructor
@AllArgsConstructor
public final class TaskDTOService implements ITaskDTOService {

    @Nullable
    @Autowired
    private TaskDTORepository repository;

    @Nullable
    @Override
    public List<TaskDTO> findAll() throws Exception {
        return repository.findAll();
    }

    @Override
    @Transactional
    public void removeById(@Nullable final String id) throws Exception {
        if (id == null || id.isEmpty()) throw new IdEmptyException();
        repository.deleteById(id);
    }

    @NotNull
    @Override
    @Transactional
    public TaskDTO add(@NotNull final TaskDTO model) throws Exception {
        return repository.save(model);
    }

    @Override
    @Transactional
    public void clear() throws Exception {
        repository.deleteAll();
    }

    @Override
    public boolean existsById(@NotNull final String id) throws Exception {
        if (id == null || id.isEmpty()) return false;
        return repository.existsById(id);
    }

    @Nullable
    @Override
    public TaskDTO findOneById(@NotNull final String id) throws Exception {
        if (id == null || id.isEmpty()) throw new IdEmptyException();
        @NotNull final Optional<TaskDTO> result = repository.findById(id);
        return result.orElse(null);
    }

    @Override
    @Transactional
    public void remove(@NotNull final TaskDTO model) throws Exception {
        if (model == null) return;
        repository.delete(model);
    }

    @Override
    @Transactional
    public TaskDTO update(@NotNull final TaskDTO model) throws Exception {
        if (model == null) return null;
        return repository.save(model);
    }

    @Override
    public int count() throws Exception {
        return (int) repository.count();
    }

    @Override
    @Transactional
    public void changeTaskStatusById(@Nullable final String id, @Nullable final Status status) throws Exception {
        if (id == null || id.isEmpty()) throw new IdEmptyException();
        if (status == null) throw new StatusEmptyException();
        @Nullable final TaskDTO task = findOneById(id);
        if (task == null) throw new TaskNotFoundException();
        task.setStatus(status);
        repository.save(task);
    }

    @NotNull
    @Override
    @Transactional
    public TaskDTO create(@Nullable final String name) throws Exception {
        if (name == null || name.isEmpty()) throw new NameEmptyException();
        @NotNull TaskDTO task = new TaskDTO();
        task.setName(name);
        return repository.save(task);
    }

    @NotNull
    @Override
    @Transactional
    public TaskDTO create(@Nullable final String name, @Nullable final String description) throws Exception {
        if (name == null || name.isEmpty()) throw new NameEmptyException();
        if (description == null || description.isEmpty()) throw new DescriptionEmptyException();
        @NotNull TaskDTO task = new TaskDTO();
        task.setName(name);
        task.setDescription(description);
        return repository.save(task);
    }

    @Nullable
    @Override
    public List<TaskDTO> findAllByProjectId(@Nullable final String projectId) throws Exception {
        if (projectId == null || projectId.isEmpty()) return Collections.emptyList();
        return repository.findByProjectId(projectId);
    }

    @Override
    @Transactional
    public void updateById(@Nullable final String id, @Nullable final String name, @Nullable final String description) throws Exception {
        if (id == null || id.isEmpty()) throw new IdEmptyException();
        if (name == null || name.isEmpty()) throw new NameEmptyException();
        if (description == null || description.isEmpty()) throw new DescriptionEmptyException();
        @Nullable final TaskDTO task = findOneById(id);
        if (task == null) throw new TaskNotFoundException();
        task.setName(name);
        task.setDescription(description);
        repository.save(task);
    }

    @Override
    @Transactional
    public void updateProjectIdById(@Nullable final String id, @Nullable final String projectId) throws Exception {
        if (id == null || id.isEmpty()) throw new IdEmptyException();
        @Nullable final TaskDTO task = findOneById(id);
        if (task == null) throw new TaskNotFoundException();
        task.setProjectId(projectId);
        repository.save(task);
    }

}
