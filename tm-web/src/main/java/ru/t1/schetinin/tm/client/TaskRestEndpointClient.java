package ru.t1.schetinin.tm.client;

import feign.Feign;
import org.jetbrains.annotations.NotNull;
import org.springframework.beans.factory.ObjectFactory;
import org.springframework.boot.autoconfigure.web.HttpMessageConverters;
import org.springframework.cloud.netflix.feign.support.SpringDecoder;
import org.springframework.cloud.netflix.feign.support.SpringEncoder;
import org.springframework.cloud.netflix.feign.support.SpringMvcContract;
import org.springframework.http.converter.FormHttpMessageConverter;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import ru.t1.schetinin.tm.model.dto.TaskDTO;

import java.util.List;

public interface TaskRestEndpointClient {

    @NotNull
    String BASE_URL = "http://localhost:8080/api/tasks";

    static TaskRestEndpointClient client() {
        @NotNull final FormHttpMessageConverter converter = new FormHttpMessageConverter();
        @NotNull final HttpMessageConverters converters = new HttpMessageConverters(converter);
        @NotNull final ObjectFactory<HttpMessageConverters> objectFactory = () -> converters;
        return Feign.builder()
                .contract(new SpringMvcContract())
                .encoder(new SpringEncoder(objectFactory))
                .decoder(new SpringDecoder(objectFactory))
                .target(TaskRestEndpointClient.class, BASE_URL);
    }

    @GetMapping("/findAll")
    List<TaskDTO> findAll() throws Exception;

    @GetMapping("/findAllByProjectId/{projectId}")
    List<TaskDTO> findAllByProjectId(@PathVariable("projectId") String projectId) throws Exception;

    @PostMapping("/add")
    TaskDTO add(@RequestBody TaskDTO task) throws Exception;

    @PostMapping("/save")
    TaskDTO save(@RequestBody TaskDTO task) throws Exception;

    @GetMapping("/findById/{id}")
    TaskDTO findById(@PathVariable("id") String id) throws Exception;

    @GetMapping("/existsById/{id}")
    boolean existsById(@PathVariable("id") String id) throws Exception;

    @GetMapping("/count")
    long count() throws Exception;

    @PostMapping("/deleteById/{id}")
    void deleteById(@PathVariable("id") String id) throws Exception;

    @PostMapping("/delete")
    void delete(@RequestBody TaskDTO task) throws Exception;

    @PostMapping("/deleteAll")
    void clear() throws Exception;

}
