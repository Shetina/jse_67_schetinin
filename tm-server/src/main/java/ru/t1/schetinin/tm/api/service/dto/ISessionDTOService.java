package ru.t1.schetinin.tm.api.service.dto;

import ru.t1.schetinin.tm.dto.model.SessionDTO;

public interface ISessionDTOService extends IUserOwnedDTOService<SessionDTO> {
}