package ru.t1.schetinin.tm.api.service.dto;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.t1.schetinin.tm.enumerated.Status;
import ru.t1.schetinin.tm.model.dto.ProjectDTO;

import java.util.List;

public interface IProjectDTOService {

    @Nullable
    List<ProjectDTO> findAll() throws Exception;

    void removeById(@Nullable String id) throws Exception;

    @NotNull
    ProjectDTO add(@NotNull ProjectDTO model) throws Exception;

    void clear() throws Exception;

    boolean existsById(@NotNull String id) throws Exception;

    @Nullable
    ProjectDTO findOneById(@NotNull String id) throws Exception;

    void remove(@NotNull ProjectDTO model) throws Exception;

    ProjectDTO update(@NotNull ProjectDTO model) throws Exception;

    void changeProjectStatusById(@Nullable String id, @Nullable Status status) throws Exception;

    @NotNull
    ProjectDTO create(@Nullable String name) throws Exception;

    @NotNull
    ProjectDTO create(@Nullable String name, @Nullable String description) throws Exception;

    void updateById(@Nullable String id, @Nullable String name, @Nullable String description) throws Exception;

    int count() throws Exception;

}
